import csv
import os
from os.path import isfile

from keras.models import Sequential, model_from_yaml
from keras.layers import Dense, Dropout, LeakyReLU, GaussianNoise
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.utils import to_categorical
from keras.metrics import categorical_accuracy
import numpy as np


def get_training_data():
    train_x = []
    train_Y = []
    ind_to_str = []
    ind = 0
    for path in os.listdir('./data/'):
        if not isfile('./data/{}'.format(path)):
            continue
        label = path[:-4]
        ind_to_str.append(label)
        reader = csv.reader(open('./data/{}'.format(path), 'r', newline=''))
        next(reader, None)
        i = 1
        r = []
        for row in reader:
            # train_x.append(np.array(row[1:]))
            r += row[1:]
            if i % 8 == 0:
                train_x.append(np.array(r))
                r = []
                train_Y.append(ind)
            i += 1
        ind += 1
    return np.array(train_x), np.array(train_Y), ind_to_str


train_x, train_Y, ind_to_str = get_training_data()

print(train_x.shape)
print(train_Y.shape)
print(ind_to_str)

model = Sequential()

model.add(Dense(64, activation='sigmoid', input_dim=64))
# model.add(LeakyReLU(alpha=0.1))
model.add(Dropout(0.5))

# model.add(GaussianNoise(3))
# model.add(Dense(256))
model.add(LeakyReLU(alpha=0.1))
model.add(Dropout(0.5))

model.add(Dense(32, activation='sigmoid'))
# model.add(LeakyReLU(alpha=0.1))
model.add(Dropout(0.5))

model.add(Dense(len(ind_to_str), activation='softmax'))

# yaml = open('./model.yml', 'r').read()
# model = model_from_yaml(yaml)

model.compile(optimizer=RMSprop(lr=0.0005),
              loss='binary_crossentropy',
              metrics=['accuracy']
              )

one_hot_labels = to_categorical(train_Y, num_classes=len(ind_to_str))
model.fit(train_x,
          one_hot_labels,
          epochs=2000,
          batch_size=30,
          validation_split=0.2,
          shuffle=True,
          callbacks=[
              ModelCheckpoint('./models/model.{epoch:02d}-{val_loss:.2f}.hdf5',
                              save_best_only=True),
              EarlyStopping(patience=10)
          ])

model.save_weights('./models/model.h5')
