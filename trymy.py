import time
from datetime import datetime, timedelta
import requests
import myo as libmyo
import numpy as np
import os
from keras.models import load_model

libmyo.init()
feed = libmyo.device_listener.Feed()
hub = libmyo.Hub()
hub.run(1000, feed)

myo = feed.get_connected_devices()[0]
myo.set_stream_emg(libmyo.StreamEmg.enabled)
# states = ['can', 'glasses', 'folder', 'none']
# states = ['can', 'phone', 'folder', 'none']

# model.78-0.35.hdf5
# states = ['phone', 'folder', 'none']

# model.best.hdf5
# states = ['fist', 'none', 'can', 'spread']

# pencil, phone, none
# model.50-0.40.hdf5
# states = ['pencil', 'phone', 'none']

# model.31-0.26.hdf5
states = ['Not Can', 'Can']

model = load_model('./models/model.31-0.26.hdf5')

"""
i = 1
r = []
for row in reader:
    r += row[1:]
    if i % 8 == 0:
        train_x.append(np.array(r))
        r = []
        train_Y.append(ind)
"""

TOLERANCE = 5
can = False
can_count = 0
not_can_count = 0
start_time = None
history = []

try:
    i = 1
    data = []
    aggregate = np.zeros(4)
    while True:
        myos = feed.get_connected_devices()
        if myos:
            data += myos[0].emg
            if i % 8 == 0:
                #print(myos[0], myos[0].emg)
                #print(len(data))
                pred = model.predict(np.array([np.array(data)]))
                ind = np.argmax(pred)
                # ind = 0 if (pred[0] > .3).any() else 1
                aggregate[ind] += 1
                data = []
            if i % 25 == 0:
                #print(states[np.argmax(aggregate)])
                if np.argmax(aggregate) == 1:
                    can_count += 1
                    not_can_count = 0
                else:
                    not_can_count += 1
                    can_count = 0
                if not can and can_count >= TOLERANCE:
                    can = True
                    not_can_count = 0
                    start_time = datetime.now()
                    print("Holding: {}".format(can))
                    #send post request to the server telling them that they have started canning
                    r = requests.post("http://breadcrumbs.sites.tjhsst.edu/cannotcan", data = {"hold": "true", "time": start_time})

                if can and not_can_count >= TOLERANCE:
                    can = False
                    end_time = datetime.now()
                    history.append((start_time, end_time))
                    print("Holding: {}".format(can))
                    print("Held object for {} units of time".format(end_time - start_time))
                    can_count = 0
                    r = requests.post("http://breadcrumbs.sites.tjhsst.edu/cannotcan", data = {"hold": "false", "time": end_time})
                    print(r.text)

                aggregate = np.zeros(4)
            i += 1
        time.sleep(0.01)
finally:
    hub.stop(True)
    hub.shutdown()
